"use strict";

let number = +prompt("Enter a number");
let noNumbers = true;

while (!Number.isInteger) {
  number = +prompt("Enter a number");
}

for (let i = 0; number > i; i++) {
  if (i > 0 && !(i % 5)) {
    console.log(i);
    noNumbers = false;
  }
}

if (noNumbers) {
  console.log("sorry no number");
}
